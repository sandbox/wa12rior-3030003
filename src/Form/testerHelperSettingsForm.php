<?php
namespace Drupal\tester_helper\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class testerHelperSettingsForm.
 *
 * @package Drupal\tester_helper\Form
 */
class testerHelperSettingsForm extends ConfigFormBase {

  /** @var string Config settings */
  const SETTINGS = 'tester_helper.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'test_helper_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * Deletes old tester user.
   *
   * @param string $user - username.
   */
  private function deleteUser(string $user) {
    $user = user_load_by_name($user);

    if ($user) {
      user_delete($user->id());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['label'] = [
      '#type' => 'label',
      '#title' => $this->t('Note: changing configuration will remove old tester account with its content.'),
    ];

    // fields.
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Testing Account Names (e.g. tester, operator, messenger)'),
      '#default_value' => $config->get('name'),
    ];

    $form['mail'] = [
      '#type' => 'textfield',
      '#title' => $this->t('E-mail'),
      '#default_value' => $config->get('mail'),
    ];

    $form['token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Token (read-only, is also password)'),
      '#default_value' => $config->get('token'),
      '#attributes' => ['readonly' => 'readonly'],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Retrieves the configuration and set
   * the submitted configuration setting
   *
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $usersToDelete = explode(',', $config->get('name'));
    foreach ($usersToDelete as $user) {
      $this->deleteUser(trim($user) . $config->get('token'));
    }
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('name', $form_state->getValue('name'))
      ->set('mail', $form_state->getValue('mail'))
      ->save();
    parent::submitForm($form, $form_state);
  }
}