<?php

/**
 * @file
 * Contains tester_helper.module.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_help().
 * @param string $route_name
 * Name of the root.
 * @return string $output
 * Returns output string in help.
 */
function tester_helper_help(string $route_name) {
  switch ($route_name) {
		  case 'help.page.tester_helper':
        $text =
          <<<EOT
          <ol><li>In registration form create account testerXXXX, where XXXX is your token (e.g. tester2137) which you can find under: </br> <b>Configuration</b> > <b>Development</b> > <b>Manage tester users</b></br> or in the link below. </li>
          <li>You can create user with certain role when you type testerXXXX.role.</li>
          <li>User can have multiple roles on tester, just add them at the end .role.administrator.operator etc.<br>
          <li>You can create multiple tester users. In the configuration menu just type them in the account names separated with comma. (Example: tester, operator)<br>
          </ol> 

          <h4>If you want to assign role that doesn't exist you will get informed by warning.</h4>
EOT;
        $output = '<h3>' . t('How to use module') . '</h3>';
        $output .= '<p>' . t($text) . '</p>';
        return $output;

    default:
  }

  return false;
}


/**
 * @param $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 * @param $form_id
 */
function tester_helper_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  // user registration form
  if ($form_id == 'user_register_form') {
//    array_unshift($form['#validate'], 'tester_helper_validate_user');
    array_unshift($form['actions']['submit']['#submit'], 'tester_helper_submit_user');
  }
}

/**
 * Custom validate function.
 * Deletes the user.
 */
//function tester_helper_validate_user() {
//  $config = \Drupal::config('tester_helper.settings');
//  deleteUser($config->get('name') . $config->get('token'));
//}

/**
 * Custom submit function.
 *
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 */
function tester_helper_submit_user($form, FormStateInterface &$form_state) {
  $config = \Drupal::config('tester_helper.settings');
  $token = $config->get('token');
  $userName = $form_state->getUserInput()['name'];

  /**
   * Splits name with delimiter .
   * for specifying user roles
   */

  $userName = explode('.', $userName) ? explode('.', $userName) : array($userName);
  $roles = $userName;
  array_shift($roles);

  /**
   * If the user is a tester
   * he has been changed username
   * mail and password demanded in
   * tester_helper.settings.yml
   */

  if ($user = isUserATester($userName[0], $config, $token)) {
    $credentials = [
      $user . $token,
      $config->get('mail')
    ];

    deleteUser($credentials[0]);

    changeCredentials($credentials, $form_state, $token);

    changeRoles($form_state, $roles);

    activateUser($form_state);
  }
}

/**
 * Checks if user is tester user.
 *
 * @param string $user
 * @param \Drupal::config $config
 * @param string $pin
 *
 * @return bool
 */
function isUserATester(string $user, $config, string $pin) {
    $testers = explode(',' , $config->get('name'));
    foreach ($testers as $tester) {
      if ($user == trim($tester) . $pin) {
       return trim($tester);
      }
    }

    return false;
}

/**
 * Changes roles of the user specified
 * in the name query.
 *
 * @param $formState
 * @param array $roles
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 */
function changeRoles(FormStateInterface $formState, array $roles) {
  $createdRoles = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple();
  foreach ($createdRoles as $index => $createdRole) {
    $cr_roles[] = $index;
  }

  array_shift($cr_roles);

  array_unshift($roles, $formState->getValue('roles')[0]);
  $formState->setValue('roles', $roles);
  $rolesDifference = array_diff($roles, $cr_roles);
  if ($rolesDifference) {
    foreach ($rolesDifference as $diff) {
      \Drupal::messenger()->addWarning('Role ' . $diff . " doesn't exist.");
    }
  }
}

/**
 * Deletes specified user.
 *
 * @param string $user
 */
function deleteUser(string $user) {
    $user = user_load_by_name($user);

    if ($user) {
      user_delete($user->id());
    }
}

/**
 * Activates new user.
 *
 * @param \Drupal\Core\Form\FormStateInterface $formState
 */
function activateUser(FormStateInterface $formState) {
  $formState->setValue('status', '1');
}

/**
 * Changes new user's email,
 * name and password(token).
 *
 * @param array $credentials
 * @param string $token
 * @param \Drupal\Core\Form\FormStateInterface $formState
 */
function changeCredentials(array $credentials, FormStateInterface $formState, string $token) {
  $formState->setValue('name', $credentials[0]);
  if ($token) {
    $formState->setValue('pass', $token);
  }
  $formState->setValue('mail', $credentials[0] . $credentials[1]);
}
